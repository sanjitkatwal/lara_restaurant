<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;

class BaseController extends Controller
{
    public function loadCommonDataToView($view_path)
    {
        // return $view_path;
        // dd($this->base_route);
        View::composer($view_path, function ($view){
            $view->with('dashboard_url', route('admin.dashboard'));
            $view->with('_base_route', $this->base_route);
            $view->with('_view_path', $this->view_path);
            $view->with('_panel', $this->panel);
        });

        return $view_path;
   }

   public function rowExist($row)
    {
        if(!$row){
            request()->session()->flash('error_message', 'Invalid request');
            return redirect()->route($this->base_route)->send();
        }
        
   }
}
