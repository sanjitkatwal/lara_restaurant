@extends('admin.auth.layout')

@section('title')
    Login
    @endsection

@section('style')
    <style>
        .login-wrapper{
            border: 1px solid #56b395;
            padding: 20px 20px;
            border-radius: 3px;
        }
    </style>
@endsection

@section('content')
<div class="middle-box text-center loginscreen animated fadeInDown">
    <div class="login-wrapper">

        <p>Welcome To Neputer Attendance</p>
        <form class="m-t" method="POST" action="{{ route('login') }}">
            @csrf
            <div class="form-group">
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}"  autocomplete="email" autofocus>

                @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group">
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary block full-width m-b">Login</button>
            <a href="{{ route('register') }}" class="float-right"><small>Account Request</small></a>
            <a href="{{ route('password.request') }}" class="float-left"><small>Forgot Password</small></a>
            <p class="clear"></p>
        </form>
    </div>
</div>
@endsection
