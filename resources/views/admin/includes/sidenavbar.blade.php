<nav class="navbar-default navbar-static-side" role="navigation">
<div class="sidebar-collapse">
<ul class="nav metismenu" id="side-menu">
    <li class="nav-header">
        <div class="dropdown profile-element">
            <img alt="image" class="rounded-circle" src="{{ asset('assets/admin/images/profile_small.jpg') }}"/>
            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                <span class="block m-t-xs font-bold">David Williams</span>
                <span class="text-muted text-xs block">Art Director <b class="caret"></b></span>
            </a>
            <ul class="dropdown-menu animated fadeInRight m-t-xs">
                <li><a class="dropdown-item" href="profile.html">Profile</a></li>
                <li><a class="dropdown-item" href="contacts.html">Contacts</a></li>
                <li><a class="dropdown-item" href="mailbox.html">Mailbox</a></li>
                <li class="dropdown-divider"></li>
                <li><a class="dropdown-item" href="login.html">Logout</a></li>
            </ul>
        </div>
        <div class="logo-element">
            IN+
        </div>
    </li>



    <li class="">
        <a href="#" aria-expanded="false"><i class="fa fa-bar-chart-o"></i> <span class="nav-label">Category </span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse" aria-expanded="false" style="height: 0px;">
            <li><a href="">list</a></li>
            <li><a href="">add</a></li>
        </ul>
    </li>




</ul>
</div>
</nav>
