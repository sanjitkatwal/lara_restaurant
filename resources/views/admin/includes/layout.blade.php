<!-- head section -->
@include('admin.includes.head')

<body>
    <div id="wrapper">

        <!-- sidenavbar section -->
        @include('admin.includes.sidenavbar')

        <div id="page-wrapper" class="gray-bg dashbard-1">

            <!-- Topnavbar section -->
            @include('admin.includes.topnavbar')


            <!-- Breadcrumb -->
            @include('admin.includes.breadcrumb')


            <!-- content/body section -->
            @yield('content')

            <!-- this is footer section -->
            @include('admin.includes.footer')

        </div>

    </div>

<!-- footer section -->
@include('admin.includes.script')
</body>
</html>
