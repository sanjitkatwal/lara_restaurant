@if(Request::segment(2) =='category' || Request::segment(2) =='dashboard' || Request::segment(2) =='product' || Request::segment(2) =='record'  )
<div class="row wrapper border-bottom white-bg page-heading">
<div class="col-lg-10">
    <h2>dashboard</h2>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="">Home</a>
        </li>
        <li class="breadcrumb-item">
            <a>Forms</a>
        </li>
        <li class="breadcrumb-item active">
            <strong>Basic Form</strong>
        </li>
    </ol>
</div>
<div class="col-lg-2">
</div>
</div>
@else
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>{{ $_folder }} > List</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route($_dashboard) }}">home</a>
                </li>
                <li class="breadcrumb-item">
                    <a>{{ $_folder }}</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>List</strong>
                </li>
            </ol>
        </div>
        

        
    </div>

@endif
